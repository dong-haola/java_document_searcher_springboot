package com.example.demo.DocSearcherController;

import com.example.demo.searcher.DocSearcher;
import com.example.demo.searcher.Result;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with Intellij IDEA.
 * Description:
 * User: DELL
 * Date: 2023-01-26
 * Time: 12:50
 */
@RestController
public class DocSearcherController {

    ObjectMapper objectMapper = new ObjectMapper();

     DocSearcher docSearcher = new DocSearcher();

    @RequestMapping("/searcher")
    public String search(@RequestParam("query") String query) throws JsonProcessingException {
        List<Result> results = docSearcher.search(query);
        return objectMapper.writeValueAsString(results);
    }
}
