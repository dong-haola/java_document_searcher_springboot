package com.example.demo.searcher;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with Intellij IDEA.
 * Description:
 * User: DELL
 * Date: 2023-01-11
 * Time: 11:33
 */
public class Index{
    //索引文件保存路径
    private static String INDEX_PATH = null;

    static {
        if(Config.isOnline){
            INDEX_PATH = "/root/tomcat/dos-searcher-index/";
        }else {
            INDEX_PATH = "E:\\dos_searcher_index";
        }
    }

    private ObjectMapper objectMapper=new ObjectMapper();

    //使用数组下标表示 docId
    private ArrayList<DocInfo> forwardIndex = new ArrayList<>();
    //构建两个锁对象来进行加锁
    private Object locker1 =new Object();//正排锁
    private Object locker2 =new Object();//倒排所
    //这里用哈希表 来表示倒排索引结构
    private HashMap<String , ArrayList<Weight>>invertedIndex = new HashMap<>();

    //1.给定一个docId, 在正排索引中, 查询文档信息; 复杂度O(1)
    public DocInfo getDocInfo(int docId){
        return forwardIndex.get(docId);
    }


    //2.根据词, 在倒排索引中, 查哪些文档和这个词相关联; 复杂度O(1)
    //这里返回值是一个与这个词相关联的文章链表
    public List<Weight> getInverted(String term){
        return invertedIndex.get(term);
    }


    //3.往索引中添加文档
    public void addDoc(String title , String url, String content){
        //添加文档时正排索引和倒排索引都要新增信息
        //构建正排索引
        DocInfo docInfo= builderForward(title,url,content);
        //构建倒排索引
        builderInverted(docInfo);
    }

    private DocInfo builderForward(String title, String url, String content) {
        DocInfo docInfo=new DocInfo();
        docInfo.setContent(content);
        docInfo.setUrl(url);
        docInfo.setTitle(title);
        synchronized (locker1){
            docInfo.setDcoId(forwardIndex.size());
            forwardIndex.add(docInfo);
        }

        return docInfo;
    }

    private void builderInverted(DocInfo docInfo) {

        class WordCont{
            public int titleCount;
            public int contentCount;
        }

        //用一个HashMap来统计词频
        HashMap<String, WordCont> wordContHashMap = new HashMap<>();

        //1.针对文档标题进行分词
        List<Term> terms = ToAnalysis.parse(docInfo.getTitle()).getTerms();
        //2.遍历分词结果,统计每个词出现的次数
        for(Term term: terms){
            //先判定term是否存在
            String word = term.getName();
            WordCont wordCont = wordContHashMap.get(word);
            if(wordCont==null){
                //如果不存在,就创建一个新的键值对, 插入进去, titleCount
                WordCont newWordCnt = new WordCont();
                newWordCnt.contentCount=0;
                newWordCnt.titleCount=1;
                wordContHashMap.put(word,newWordCnt);
            }else{
                //如果存在, 就找到之前的值, 把对应的titleCount+1
                wordCont.titleCount += 1;
            }
        }

        //3.针对文章正文进行分词
        terms = ToAnalysis.parse(docInfo.getContent()).getTerms();
        //4.遍历分词结果,统计每个次出现的次数
        for(Term term:terms){
            String word = term.getName();
            WordCont wordCont=wordContHashMap.get(word);
            if(wordCont==null){
                WordCont newWordCont = new WordCont();
                newWordCont.titleCount=0;
                newWordCont.contentCount=1;
                wordContHashMap.put(word,newWordCont);
            }else {
                wordCont.contentCount+=1;
            }
        }

        //5.汇总上述的到的结果,放到一个HashMap中
        //  权重的计算简单设定为 weight= 标题中出现的次数 * 10 + 正文中出现的次数
        //6.遍历得到的 HashMap, 依次来更新倒排索引的结构
        //此处需要将Map转换为Set来遍历
        for(Map.Entry<String, WordCont> entry : wordContHashMap.entrySet()){

            synchronized (locker2){
                //先根据词查,在倒排索引中查一下
                //倒排拉链
                List<Weight> invertedList = invertedIndex.get(entry.getKey());
                if(invertedList==null){

                    //如果为空的话,构建一个新的键值对
                    ArrayList<Weight> newInvertedIndex = new ArrayList<>();

                    //把当前文档构建成Weight对象,插入
                    Weight weight = new Weight();
                    weight.setDocId(docInfo.getDcoId());
                    weight.setWeight(entry.getValue().titleCount * 10 + entry.getValue().contentCount);

                    newInvertedIndex.add(weight);
                    invertedIndex.put(entry.getKey(),newInvertedIndex);
                }else {
                    Weight weight = new Weight();
                    weight.setDocId(docInfo.getDcoId());
                    weight.setWeight(entry.getValue().titleCount * 10 + entry.getValue().contentCount);
                    invertedList.add(weight);
                }
            }


        }
    }

    //4.把索引添加到磁盘中
    public void save(){
        long beg=System.currentTimeMillis();
        System.out.println("保存索引开始!");
        //使用两个文件来保存正排和倒排
        //1.先判断一下索引对应的目录是否存在
        File indexPathFile = new File(INDEX_PATH);
        if( !indexPathFile.exists()){
            indexPathFile.mkdirs();
        }
        File forwardIndexFile = new File(INDEX_PATH+"forward.txt");
        File invertedIndexFile = new File(INDEX_PATH+"inverted.txt");
        try{
            objectMapper.writeValue(forwardIndexFile, forwardIndex);
            objectMapper.writeValue(invertedIndexFile, invertedIndex);
        }catch (IOException e){
            e.printStackTrace();
        }
       long end=System.currentTimeMillis();
     System.out.println("保存索引完成! 消耗的时间: " + (end - beg) + " ms");
    }

    //5.把磁盘中的索引加载到内存中
    public void load(){
        long beg=System.currentTimeMillis();
        System.out.println("加载索引开始!");
        //设置加载路径
        File forwardIndexFile = new File(INDEX_PATH+ "forward.txt");
        File invertedIndexFile = new File(INDEX_PATH+ "inverted.txt");
        try{
            //创建了一个匿名内部类实例,把ArrayList<searcher.DocInfo> 这个类型信息,告诉readValue方法
            forwardIndex = objectMapper.readValue(forwardIndexFile, new TypeReference<ArrayList<DocInfo>>() {});
            invertedIndex = objectMapper.readValue(invertedIndexFile, new TypeReference<HashMap<String, ArrayList<Weight>>>() {});
        }catch (IOException e){
            e.printStackTrace();
        }
        long end=System.currentTimeMillis();
        System.out.println("保存索引完成! 消耗的时间: " + (end - beg) + " ms");
    }

    public static void main(String[] args) {
        Index index = new Index();
        index.load();
        System.out.println("加载索引完成");
    }
}
