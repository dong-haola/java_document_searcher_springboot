package com.example.demo.searcher;

/**
 * Created with Intellij IDEA.
 * Description:
 * User: DELL
 * Date: 2023-01-11
 * Time: 11:33
 */

public class DocInfo {
    private String title;
    private String content;
    private int dcoId;
    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getDcoId() {
        return dcoId;
    }

    public void setDcoId(int dcoId) {
        this.dcoId = dcoId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
