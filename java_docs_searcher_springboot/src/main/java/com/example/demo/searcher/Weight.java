package com.example.demo.searcher;

/**
 * Created with Intellij IDEA.
 * Description:
 * User: DELL
 * Date: 2023-01-11
 * Time: 11:39
 */
public class Weight {
    public int getDocId() {
        return docId;
    }

    public void setDocId(int docId) {
        this.docId = docId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    private int docId;

    private int weight;//weight 表示文档与词之间的相关性
}
