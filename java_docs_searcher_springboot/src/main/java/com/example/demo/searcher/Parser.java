package com.example.demo.searcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created with Intellij IDEA.
 * Description:
 * User: DELL
 * Date: 2023-01-10
 * Time: 13:38
 */
public class Parser {
    private static final String INPUT_PATH="E:/dos_searcher_index/docs/";

    private Index index = new Index();
    //创建函数的入口
    //单个线程解析制作索引
    public  void run(){
        //根据上面路径枚举出所有文件(html), 封装枚举函数
        //针对罗列文件路径,打开文件, 读取内容, 并进行解析, 构建索引
        //把构造好的索引数据结构, 保存到指定文件中
        ArrayList<File> fileList = new ArrayList<>();
        enumFile(INPUT_PATH,fileList);
//        System.out.println(fileList);
//        System.out.println(fileList.size());

        for(File f:fileList){
            //解析HTML函数
            System.out.println("开始解析: " + f.getAbsolutePath());
            parseHTML(f);
        }

        // 把 内存中构造的索引数据结构, 保存到指定的文件中去
       index.save();
    }

    public void runByThread() throws InterruptedException {
        long beg = System.currentTimeMillis();
        System.out.println("制作索引开始!");
        //1.根据路径枚举文件(.html)
        ArrayList<File> files =new ArrayList<>();
        enumFile(INPUT_PATH,files);
        //2.循环遍历解析文件,引入线程池
        //引入CountDownLatch 来防止,未解析完文件就直接保存到磁盘中
        CountDownLatch latch =new CountDownLatch(files.size());
        ExecutorService executorService = Executors.newFixedThreadPool(6);
        for (File f:files){
            executorService.submit(new Runnable() {
                @Override
                public void run() {
                    System.out.println("解析 " + f.getAbsolutePath());
                    parseHTML(f);
                    latch.countDown();
                }
            });
        }
        // 调用await 方法会阻塞, 直到所有线程都调用 countDown 结束之后,才结束阻塞
        latch.await();
        //手动把线程池里的线程都干掉
        executorService.shutdown();
        index.save();
        long end = System.currentTimeMillis();
        System.out.println("索引制作完成! 消耗的时间: " + (end - beg) + " ms");


    }



    private void parseHTML(File f) {
        //1.解析标题
        String title=parseTitle(f);
        //2.解析URL()
        String url=parseUrl(f);
        //3.解析正文
        //String content=parsContent(f);
        String content = parsContentByregex(f);
        //4.把解析的数据加入到索引中
        index.addDoc(title,url,content);
    }

    private String parseTitle(File f) {
        //结束的位置: 总长度-".html"长度
        //起始位置: 0
        return f.getName().substring(0,(f.getName().length())-(".html".length()));
    }
    private String parseUrl(File f) {

        String part1 = "https://docs.oracle.com/javase/8/docs/";
        String part2 = f.getAbsolutePath().substring(INPUT_PATH.length());
        return part1+part2;
    }



    private String parsContentByregex(File f){
        //1.把整个文件都读到String中去
        String content = readFile(f);
        //2.替换掉script标签
        content = content.replaceAll("<script.*?>(.*?)</script>", " ");
        //3.替换掉普通 html标签
        content = content.replaceAll("<.*?>", " ");
        //4.使用正则把空格合并
        content = content.replaceAll("\\s+", " ");
    return content;
    }

    private String readFile(File f) {
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(f))){
            StringBuilder content = new StringBuilder();
            while (true){
                int ret = bufferedReader.read();
                if(ret==-1){
                    break;
                }
                char c =(char) ret;

                if(c=='\n' || c=='\r'){
                    //为了去掉换行和空格
                    c = ' ';
                }
                content.append(c);
            }
            return content.toString();
        }catch (IOException e){
            e.printStackTrace();
        }
        return "";
    }


    private String parsContent(File f) {
        //将文件按照字符一个一个读取
        try(BufferedReader bufferedReader = new BufferedReader(new FileReader(f),1024*1024)) {//手动设置缓冲区大小
            //设置一个拷贝开关
            Boolean copySwith = true;
            //设置一个拷贝字符串StringBulider
            StringBuilder content = new StringBuilder();
            while(true){
                //filRead.read(),返回值就是一个int, 不是char
                //使用int是为了防止一些非法的情况!
                //当读到了文章末尾的时候, 继续读, 就会返回-1
                int ret= bufferedReader.read();
                if(ret == -1){
                    break;
                }
                //如果不是-1, 则是一个合法的字符
                char c = (char)ret;
                if(copySwith){
                    //拷贝开关打开, 遇到普通字符就拷贝
                    if(c == '<'){
                        //关闭开关
                        copySwith = false;
                        continue;
                    }

                    if(c=='\n' || c=='\r'){
                        //为了去掉换行和空格
                        c = ' ';
                    }
                    //其他字符直接拷贝
                    content.append(c);
                }else {
                    if(c=='>'){
                        //开关打开
                        copySwith=true;
                    }
                }
            }
            return content.toString();
        }  catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    //第一个参数是从哪个目录开始查
    //第二个参数是递归得到的结果
    private void enumFile(String path, ArrayList<File> fileList){

        //先把字符串路径封装成一个文件
        File rootFile=new File(path);

        //listFiles()可以获取到当前目录下获取的文件或目录,但是只能获取到一层
        //所以需要递归遍历打印所有的子目录
        File[] files=rootFile.listFiles();
        for (File f: files) {
            //根据f类型判定, 如果是目录进入递归,不是加入fileList结果列表
            if(f.isDirectory()){
                enumFile(f.getAbsolutePath(),fileList);
            }else {
                if(f.getAbsolutePath().endsWith(".html")){
                    fileList.add(f);
                }
            }
        }
    }
    public static void main(String[] args) throws InterruptedException {

        Parser parser=new Parser();
       // parser.run();
        parser.runByThread();
    }
}
