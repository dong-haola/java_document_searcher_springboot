package com.example.demo.searcher;

import org.ansj.domain.Term;
import org.ansj.splitWord.analysis.ToAnalysis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Created with Intellij IDEA.
 * Description:
 * User: DELL
 * Date: 2023-01-12
 * Time: 10:31
 */
public class DocSearcher {

    private static  String STOP_WORD_PATH = null;
    static {
        if(Config.isOnline){
            STOP_WORD_PATH = "/root/tomcat/dos-searcher-index/StopWord.txt";
        }else {
            STOP_WORD_PATH ="E:\\dos_searcher_index\\StopWord.txt";
        }
    }
    private HashSet<String> stopWords = new HashSet<>();
    //引入索引实例, 完成索引加载到内存的操作
    private Index index =new Index();
    public  DocSearcher(){
        index.load();
        loadStopWords();
    }

    public List<Result> search(String query){
//        1. 分词
        List<Term> oldTerms = ToAnalysis.parse(query).getTerms();
        List<Term> terms = new ArrayList<>();
        //针对分词结果,使用暂停词进行分词
        for(Term term:oldTerms){
            if(stopWords.contains(term.getName())){
                continue;
            }
            terms.add(term);
        }
//        2. 触发
        List<List<Weight>> ternResult = new ArrayList<>();
        //遍历分词列表, 构建倒排拉链
        for(Term term : terms){
            String word = term.getName();
            //虽然倒排索引中有很多词,但这里的词一定都是之前的文档中存在的词
            List<Weight> invertedList = index.getInverted(word);
            if(invertedList == null){
                //说明这个词在所有文档中都不存在
                continue;
            }
            //把查询结果汇总起来
            ternResult.add(invertedList);
        }
        //3.合并
        List<Weight> allTernResult = mergeResult(ternResult);
//       4. 排序---->按照权重进行排序
        //指定比较规则,比较器
        allTernResult.sort(new Comparator<Weight>() {
            @Override
            public int compare(Weight o1, Weight o2) {
                //降序排序
                return o2.getWeight()-o1.getWeight();
            }
        });
//        5. 包装结果---> 遍历排序结果 ,查正排索引, 构造返回数据
        List<Result> results = new ArrayList<>();
        for(Weight weight : allTernResult){
            DocInfo docInfo = index.getDocInfo(weight.getDocId());
            Result result = new Result();
            result.setUrl(docInfo.getUrl());
            result.setTitle(docInfo.getTitle());
            result.setDesc(GenDesc(docInfo.getContent(),terms));
            results.add(result);

        }

        return results;
    }

    static class Pos{
        public int row;
        public int col;

        public Pos(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }
    private List<Weight> mergeResult(List<List<Weight>> source) {

        //在合并时候,需要操作一个二维List,就需要明确知道行和列,这里构造一个内部类来进行统一处理
        //1.先对每一行按照 id 进行升序排序
        for(List<Weight> curRow : source){
            curRow.sort(new Comparator<Weight>() {
                @Override
                public int compare(Weight o1, Weight o2) {
                    return o1.getDocId() - o2.getDocId();
                }
            });
        }
        //2.借助一个优先队列,针对这些行进行合并
        //targe 表示合并结果
        List<Weight> target = new ArrayList<>();
        //构建一个优先级队列设定比较规则(按照DocId ,小的优先)
        PriorityQueue<Pos> queue = new PriorityQueue<>(new Comparator<Pos>() {
            @Override
            public int compare(Pos o1, Pos o2) {
                Weight w1= source.get(o1.row).get(o1.col);
                Weight w2= source.get(o2.row).get(o2.col);
                return w1.getDocId()-w2.getDocId();
            }
        });

        //初始化队列,把每一行第一个元素放到队列中去
        for(int row = 0; row<source.size();row++){
            //初始插入到元素的col就是0
            queue.offer(new Pos(row, 0));
        }
        //循环取队首元素
        while (!queue.isEmpty()){
            Pos minPos = queue.poll();
            Weight cueWeight = source.get(minPos.row).get(minPos.col);
            //比较取到的Weight 是否和前一个插入到 target 中的结果是相同的 docId ; 相等就合并
            if(target.size() > 0){
                Weight lastWeight = target.get(target.size() -1);
                if(lastWeight.getDocId() == cueWeight.getDocId()){
                    lastWeight.setWeight(lastWeight.getWeight() + cueWeight.getWeight());
                }else {
                    //不相等在则直接插入
                    target.add(cueWeight);
                }
            }else {
                //如果target 当前是空的直接插入就行
                target.add(cueWeight);
            }
            //处理完当前元素后,将光标后移处理下一个元素
            Pos newPos = new Pos(minPos.row, minPos.col+1);
            if(newPos.col >= source.get(newPos.row).size()){
                //说明到达这一行末尾,这一行处理完毕

                continue;
            }
            queue.offer(newPos);
        }
    return target;
    }

    private String GenDesc(String content, List<Term> terms) {

        int firstPos=-1;
        //遍历分词结果, 查询哪个结果在文章中存在
        for(Term term:terms){
            //此处要将正文也转换成小写再进行匹配
            String word = term.getName();
            content = content.toLowerCase().replaceAll("\\b" + word + "\\b"," " + word + " ");
            firstPos = content.indexOf(" " + word + " ");

            if(firstPos>=0){
                //找到位置
                break;
            }

        }
        if(firstPos == -1){
            //表示所有的分词结果在正文中都不存在
            //取正文的前160个字符作为描述
            if(content.length()>160){
                return content.substring(0,160)+ "...";
            }
            return content;
        }
        String desc = "";
        int descBeg = firstPos<60 ? 0 : firstPos-60;
        if(descBeg +160 > content.length()){
            desc = content.substring(descBeg);
        }else {
            desc = content.substring(descBeg,descBeg+160) + "...";
        }

        //在此处加上替换操作实现标红逻辑
        //把描述中的和分词结果相同的部分,加上i标签
        for(Term term: terms){
            String word = term.getName();
            //此处进行全字匹配
           desc = desc.replaceAll("(?i) " + word + " "," <i>" + word + " </i>");

        }
        return desc;
    }

    public void loadStopWords(){
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(STOP_WORD_PATH))){
            while (true){
                String line = bufferedReader.readLine();
                if(line==null){
                    break;
                }
                stopWords.add(line);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        DocSearcher docSearcher =new DocSearcher();
        Scanner scanner = new Scanner(System.in);
        while(true){
            System.out.print("-> ");
            String query = scanner.next();
            List <Result> results = docSearcher.search(query);
            for(Result result : results){
                System.out.println("========================");
                System.out.println(result);
            }

        }


    }

}
